$( document ).ready(function() {

    $("#editable").click(function() {
        $("#editable").selectText();
    });

    getData();

});



function getData () {
    setInterval(function(){
        var id;
        $.ajax({
            url: 'events.php',
            type: 'POST', 
            dataType: 'json',
            success: function(events) {
                // these events are from the back-end server
                console.log("Data From Server in requested format...");
                console.log(events);

                id = processEvent(events);
                removeFlashing(id);
            }
        });
    }, 5000);
}

function getAdditionalEventInfo (eventId){
    // console.log("eventid: " + eventId);
    // var eventInfo;
    var request = $.ajax({
        url: 'getSingleEventInfo.php',
        type: 'POST', 
        data: { 'id': eventId },
        dataType: 'json',
        async: false, 
        success: function(eventInfo) {
            // console.log(eventInfo);
            result = eventInfo;
        }
    });
    return result;
}

function removeFlashing (eventId){

    if ( eventId == undefined ){
        console.log("No need to update added_flag field...");
    } else {
        console.log("removeFlashing...");
        updateEventFlag(eventId);
    }

}

function deleteEvent(Id){
    $.ajax({
        url: "deleteevent.php",
        type: "POST",
        data: { 'id': Id },
        success: function(data){
            console.log(data);
        }
    });
}

function updateEventFlag(Id){
    $.ajax({
        url: "updateevent.php",
        type: "POST",
        data: { 'id': Id },
        success: function(data){
            console.log(data);
        }
    });
}

function currentDateFormat () {
    
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth()+1; //January is 0!
    var yyyy = today.getFullYear();

    if(dd<10) { dd='0'+dd } 

    if(mm<10) { mm='0'+mm }

    // curHour = today.getHours() > 12 ? today.getHours() - 12 : (today.getHours() < 10 ? "0" + today.getHours() : today.getHours()),
    curHour = today.getHours(),
	curMinute = today.getMinutes() < 10 ? "0" + today.getMinutes() : today.getMinutes(),
	curSeconds = today.getSeconds() < 10 ? "0" + today.getSeconds() : today.getSeconds(),
	
    todayTime = curHour + ":" + curMinute + ":" + curSeconds;

    todayDate = yyyy + "-" + mm + "-" + dd;

    return  todayDate + ' ' + todayTime;

}

function processEvent(events) {
    

    currentDate = currentDateFormat();

    var arrayLength = events.length;


    $('#eventTable').empty();
    $('#eventTable').append('<tr style="background-color: black; color: white;"><td>ID</td><td>OPERATOR_ID</td><td>TIME</td></tr>');
    var rowProp = "";

    var flashingID;

    for (var i = 0; i < arrayLength; i++) {
        rowProp = '';

        var moreEventInfo;
        moreEventInfo = getAdditionalEventInfo(events[i].id);


        if ( currentDate > moreEventInfo[0].expiration_time ) {
            console.log("past date -->> will be deleted...");
            deleteEvent(events[i].id);
        } else {
            console.log("future date");
            if ( moreEventInfo[0].added_flag == 1 ){
                rowProp = ' style="background-color: red; color: white;"';
                flashingID = events[i].id;
            }
            $('#eventTable').append('<tr'+rowProp+'><td>'+events[i].id+'</td><td>'+events[i].creator_id+'</td><td>'+moreEventInfo[0].expiration_time+'</td></tr>');
        }
    }
    return flashingID;
}

function displayTime () {
    // Making 2 variable month and day
    var monthNames = [ "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December" ]; 
    var dayNames= ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"]

    // make single object
    var newDate = new Date();
    // make current time
    newDate.setDate(newDate.getDate());
    // setting date and time
    $('#Date').html(dayNames[newDate.getDay()] + " " + newDate.getDate() + ' ' + monthNames[newDate.getMonth()] + ' ' + newDate.getFullYear());

    setInterval( function() {
        // Create a newDate() object and extract the seconds of the current time on the visitor's
        var seconds = new Date().getSeconds();
        // Add a leading zero to seconds value
        $("#sec").html(( seconds < 10 ? "0" : "" ) + seconds);
    },1000);

    setInterval( function() {
        // Create a newDate() object and extract the minutes of the current time on the visitor's
        var minutes = new Date().getMinutes();
        // Add a leading zero to the minutes value
        $("#min").html(( minutes < 10 ? "0" : "" ) + minutes);
    },1000);

    setInterval( function() {
        // Create a newDate() object and extract the hours of the current time on the visitor's
        var hours = new Date().getHours();
        // Add a leading zero to the hours value
        $("#hours").html(( hours < 10 ? "0" : "" ) + hours);
    }, 1000);
}

jQuery.fn.selectText = function(){
    var doc = document;
    var element = this[0];
    console.log(this, element);
    if (doc.body.createTextRange) {
        var range = document.body.createTextRange();
        range.moveToElementText(element);
        range.select();
    } else if (window.getSelection) {
        var selection = window.getSelection();        
        var range = document.createRange();
        range.selectNodeContents(element);
        selection.removeAllRanges();
        selection.addRange(range);
    }
};