<?php

require("Log.class.php");
class DB
{
    // @object, The PDO object
    private $pdo;
    
    // @object, PDO statement object
    private $sQuery;
    
    // @array,  The database settings
    private $settings;
    
    // @bool ,  Connected to the database
    private $bConnected = false;
    
    // @object, Object for logging exceptions	
    private $log;
    
    // @array, The parameters of the SQL query
    private $parameters;
    

    public function __construct()
    {
        $this->log = new Log();
        $this->Connect();
        $this->parameters = array();
    }
    

    private function Connect()
    {
        $this->settings = parse_ini_file("settings.ini.php");
        $dsn            = 'mysql:dbname=' . $this->settings["dbname"] . ';host=' . $this->settings["host"] . '';
        try {
            // Read settings from INI file, set UTF8
            $this->pdo = new PDO($dsn, $this->settings["user"], $this->settings["password"], array(
                PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"
            ));
            
            // We can now log any exceptions on Fatal error. 
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            
            // Disable emulation of prepared statements, use REAL prepared statements instead.
            $this->pdo->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);
            
            // Connection succeeded, set the boolean to true.
            $this->bConnected = true;
        }
        catch (PDOException $e) {
            // Write into log
            echo $this->ExceptionLog($e->getMessage());
            die();
        }
    }


    public function CloseConnection()
    {

        $this->pdo = null;
    }
    

    private function Init($query, $parameters = "")
    {
        // Connect to database
        if (!$this->bConnected) {
            $this->Connect();
        }
        try {
            // Prepare query
            $this->sQuery = $this->pdo->prepare($query);
            
            // Add parameters to the parameter array	
            $this->bindMore($parameters);
            
            // Bind parameters
            if (!empty($this->parameters)) {
                foreach ($this->parameters as $param => $value) {
                    
                    $type = PDO::PARAM_STR;
                    switch ($value[1]) {
                        case is_int($value[1]):
                            $type = PDO::PARAM_INT;
                            break;
                        case is_bool($value[1]):
                            $type = PDO::PARAM_BOOL;
                            break;
                        case is_null($value[1]):
                            $type = PDO::PARAM_NULL;
                            break;
                    }
                    // Add type when binding the values to the column
                    $this->sQuery->bindValue($value[0], $value[1], $type);
                }
            }
            
            // Execute SQL 
            $this->sQuery->execute();
        }
        catch (PDOException $e) {
            // Write into log and display Exception
            echo $this->ExceptionLog($e->getMessage(), $query);
            die();
        }
        
        // Reset the parameters
        $this->parameters = array();
    }

    public function bind($para, $value)
    {
        $this->parameters[sizeof($this->parameters)] = [":" . $para , $value];
    }


    public function bindMore($parray)
    {
        if (empty($this->parameters) && is_array($parray)) {
            $columns = array_keys($parray);
            foreach ($columns as $i => &$column) {
                $this->bind($column, $parray[$column]);
            }
        }
    }


    public function query($query, $params = null, $fetchmode = PDO::FETCH_ASSOC)
    {
        $query = trim(str_replace("\r", " ", $query));
        
        $this->Init($query, $params);
        
        $rawStatement = explode(" ", preg_replace("/\s+|\t+|\n+/", " ", $query));
        
        // Which SQL statement is used 
        $statement = strtolower($rawStatement[0]);
        
        if ($statement === 'select' || $statement === 'show') {
            return $this->sQuery->fetchAll($fetchmode);
        } elseif ($statement === 'insert' || $statement === 'update' || $statement === 'delete') {
            return $this->sQuery->rowCount();
        } else {
            return NULL;
        }
    }
    

    public function lastInsertId()
    {
        return $this->pdo->lastInsertId();
    }
    


    public function beginTransaction()
    {
        return $this->pdo->beginTransaction();
    }
    

    public function executeTransaction()
    {
        return $this->pdo->commit();
    }
    


    public function rollBack()
    {
        return $this->pdo->rollBack();
    }
    

    public function column($query, $params = null)
    {
        $this->Init($query, $params);
        $Columns = $this->sQuery->fetchAll(PDO::FETCH_NUM);
        
        $column = null;
        
        foreach ($Columns as $cells) {
            $column[] = $cells[0];
        }
        
        return $column;
        
    }


    public function row($query, $params = null, $fetchmode = PDO::FETCH_ASSOC)
    {
        $this->Init($query, $params);
        $result = $this->sQuery->fetch($fetchmode);
        $this->sQuery->closeCursor(); // Frees up the connection to the server so that other SQL statements may be issued,
        return $result;
    }


    public function single($query, $params = null)
    {
        $this->Init($query, $params);
        $result = $this->sQuery->fetchColumn();
        $this->sQuery->closeCursor(); // Frees up the connection to the server so that other SQL statements may be issued
        return $result;
    }


    private function ExceptionLog($message, $sql = "")
    {
        $exception = 'Unhandled Exception. <br />';
        $exception .= $message;
        $exception .= "<br /> You can find the error back in the log.";
        
        if (!empty($sql)) {
            // Add the Raw SQL to the Log
            $message .= "\r\nRaw SQL : " . $sql;
        }
        // Write into log
        $this->log->write($message);
        
        return $exception;
    }

    public function displayCataloguePage () 
    {
        $products = $this->query("SELECT * FROM `product`;");

        $out = "";
        foreach ($products as $product){
            $out .= $this->displayProductDiv($product);
        }
        return $out;
    }

    public function getAllEvents() 
    {

        // $res = getAllGroups($db);
        // echo $res;

        $events = $this->query("SELECT `id`, `creator_id`, `expiration_time` FROM `events` ORDER BY `time_added` DESC ;");

        //convert date to str
        foreach($events as $key => $event){
            $events[$key]['expiration_time'] = strtotime($event['expiration_time']);;
        }

        return json_encode($events);

    } 

    public function getSingleEventInfo ($id) 
    {

        $event = $this->query("SELECT * FROM `events` WHERE `id` = :id;", array("id"=>$id));

        return json_encode($event);

    } 

    public function updateEventNewFlag($id) 
    {

        return $this->query("UPDATE `events` SET `added_flag` = :added_flag WHERE id = :eventId", array("added_flag"=>"0","eventId"=>$id));

    } 

    public function deleteEvent($id) 
    {

        return  $this->query("DELETE FROM `events` WHERE id = :id",array("id"=>$id)); 

    } 


    public function generateOperatorID ()
    {
        return substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyz", 5)), 0, 5);
    }

    public function displayProductDiv($product)
    {
        $out = "";
        $dimenstionsInfo = $this->checkImageOrientation($product['image']);
        //var_dump($dimenstionsInfo);
        $out = $this->displayDiv($dimenstionsInfo, $product);

        return $out;
    }

    private function displayDiv($dimension, $product){
        //var_dump($product);
        if ($dimension['orientation'] == 'landscape'){
            return $this->divLandscape($product);
        } else {
            return $this->divPortrait($product);
        }
    }

    private function divPortrait ($prod){
        $out = "";
        $out .= "<div class='singleProduct'>";
        $out .= $prod['name'];
        $out .= "</div>";
        return $out;
    }

    private function divLandscape ($prod){
        $out = "";
        $out .= "<div class='singleProduct'>";
        $out .= $prod['name'];
        $out .= "</div>";
        return $out;
    }

    private function checkImageOrientation ($image){

        list($width, $height) = getimagesize('uploadedImages/'.$image);

        $dimensions = array('width' => $width, 'height' => $height);

        if ( $width > $height ){
            return array( 'dimensions' => $dimensions, 'orientation' => 'landscape' );
        } else {
            return array( 'dimensions' => $dimensions, 'orientation' => 'portrait' );
        }
    }

    public function pageMenu($currentPageName){
        $out = "";
        $pages = $this->query("SELECT * FROM `pagecontent` order by `position` ASC;");

        foreach ( $pages as $page ){
            $displayname = explode(".", $page['pagename']);
            if ( $currentPageName == $page['pagename'] )
                $out .= "<div id='headerElementsHome'><span class='menu' style='color: #20A0D6;'>".strtoupper($displayname[0])."</span></div>";
            else
                $out .= "<div id='headerElementsHome'><a class='menu' href='".$page['pagename']."'>".strtoupper($displayname[0])."</a></div>";
        }

        return $out;
    }

    public function displayPageContent ( $currentPageName ){
        $pagecontent = $this->single("SELECT `content` FROM `pagecontent` WHERE `pagename` = :str ", array('str' => $currentPageName ) );
        return $pagecontent;
    }
}
?>
