CREATE TABLE `events` (
  `id` int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `creator_id` varchar(10) NOT NULL,
  `expiration_time` datetime NOT NULL,
  `time_added` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `added_flag` tinyint(1) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


INSERT INTO `events` (`creator_id`, `expiration_time`) VALUES
('101', '2016-09-30 20:33:44'),
('5664', '2016-09-30 13:29:13'),
('hjl03', '2016-10-30 23:20:11'),
('h3i3ew', '2016-10-01 03:04:11'),
('hjlew', '2016-09-30 13:36:11'),
('e5ubo', '2016-09-30 18:45:11');